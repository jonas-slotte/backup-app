<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('partials.head')

<body class="font-sans antialiased">
    <x-jet-banner />

    <div class="min-h-screen bg-gray-100">
        @livewire('navigation-menu')

        <!-- Page Heading -->
        @if (isset($header))
        <header class="bg-white shadow">
            <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                {{ $header ?? null }}
            </div>
        </header>
        @endif

        <!-- Page Content -->
        <main>
            {{ $slot ?? null }}
        </main>
    </div>

    @stack('modals')
    @include('partials.scripts')
</body>

</html>