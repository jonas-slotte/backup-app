<x-jet-container>
    <x-jet-success-button wire:click="showCreateModal">
        Create
    </x-jet-success-button>

    <livewire:backup-targets-table />

    <x-jet-modal wire:model="createModal">
        <div class="px-6 py-4">
            <livewire:forms.create-backup-target />
        </div>
    </x-jet-modal>
</x-jet-container>