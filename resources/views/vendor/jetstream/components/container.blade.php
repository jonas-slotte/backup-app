<div class="p-6 m-2 h-full sm:px-20 bg-white border-b border-gray-200">
    {{$slot ?? null}}
</div>