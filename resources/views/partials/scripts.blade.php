{{-- Live wire scripts --}}
@livewireScripts
{{-- Local scripts --}}
<script src="{{ mix('js/app.js') }}"></script>
{{-- stack scripts AFTER livewireScripts --}}
@stack('scripts')