<?php

use App\Models\BackupTarget;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackupConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backup_configs', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('The name of this configuration');
            $table->json('schedule')->comment("The config or cron expression for this configuration.");
            $table->json('data')->comment('The configuration options for a backup.');
            $table->foreignIdFor(BackupTarget::class)->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backup_configs');
    }
}
