<?php

namespace App\Http\Livewire;

use Livewire\Component;

class BackupTargets extends Component
{
    public $createModal = false;

    public function showCreateModal()
    {
        $this->createModal = true;
    }

    public function createBackupTarget()
    {
    }

    public function render()
    {
        return view('pages.backup-targets');
    }
}
