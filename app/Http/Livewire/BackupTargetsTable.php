<?php

namespace App\Http\Livewire;

use App\Models\BackupTarget;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

class BackupTargetsTable extends LivewireDatatable
{
    public $model = BackupTarget::class;

    public function columns()
    {
        return [
            NumberColumn::name('id')
                ->label('ID')
                ->linkTo('job', 6),
            Column::name('name')
                ->defaultSort('asc')
                ->searchable()
                ->filterable()
        ];
    }
}
