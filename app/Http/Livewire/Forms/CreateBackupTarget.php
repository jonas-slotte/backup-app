<?php

namespace App\Http\Livewire\Forms;

use App\Models\BackupTarget;
use Livewire\Component;
use Tanthammar\TallForms\Input;
use Tanthammar\TallForms\TallForm;

class CreateBackupTarget extends Component
{
    use TallForm;

    public function mount(?BackupTarget $backuptarget)
    {
        //Gate::authorize()
        $this->fill([
            'formTitle' => trans('global.create') . ' ' . trans('crud.backuptarget.title_singular'),
            'wrapWithView' => false, //see https://github.com/tanthammar/tall-forms/wiki/Wrapper-Layout
            'showGoBack' => false,
        ]);
        $this->mount_form($backuptarget); // $backuptarget from hereon, called $this->model
    }


    // Mandatory method
    public function onCreateModel($validated_data)
    {
        // Set the $model property in order to conditionally display fields when the model instance exists, on saveAndStayResponse()
        $this->model = BackupTarget::create($validated_data);
        $this->emit('refreshLivewireDatatable'); //Update parent table
    }

    // OPTIONAL method used for the "Save and stay" button, this method already exists in the TallForm trait
    public function onUpdateModel($validated_data)
    {
        $this->model->update($validated_data);
        $this->emit('refreshLivewireDatatable'); //Update parent table
    }

    public function fields()
    {
        return [
            Input::make('Name')->rules('required'),
        ];
    }
}
