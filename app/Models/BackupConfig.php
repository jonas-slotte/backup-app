<?php

namespace App\Models;

use App\Features\Backup\BackupScheduler;
use App\Features\Backup\BackupService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BackupConfig extends Model
{
    use HasFactory;

    protected $casts = [
        'schedule' => 'array',
        'schedule' => 'data',
    ];

    /**
     * Make the runner for this backup config
     */
    public function makeRunner()
    {
        /** @var BackupScheduler */
        $service = app()->make(BackupScheduler::class);
        return $service->makeSchedule($this->schedule);
    }
}
