<?php

namespace App\Features\Backup;

use App\Features\Backup\Schedules\BackupSchedule;
use Illuminate\Console\Scheduling\CallbackEvent;

class BackupRunner
{
    public function __construct(
        protected BackupSchedule $schedule
    ) {
    }
    /**
     * Apply the schedule
     */
    public function schedule(CallbackEvent $scheduleEvent)
    {
        $this->schedule->apply($scheduleEvent);
    }

    /**
     * Run the backup
     */
    public function execute()
    {
    }
}
