<?php

namespace App\Features\Backup;

use App\Models\BackupConfig;
use Illuminate\Console\Scheduling\Schedule;
use Throwable;

class BackupScheduler
{
    protected $schedules = [
        "daily" => Daily::class
    ];

    /**
     * Make the schedule type from input.
     */
    public function makeSchedule(array $input)
    {
        $type = $input["type"] ?? null;
        $data = $input["data"] ?? [];
        $class = $this->schedules[$type];

        return new $class($data);
    }

    public function runSchedule(Schedule $schedule)
    {
        $configs = BackupConfig::all();
        foreach ($configs as $config) {
            $this->runConfigSchedule($config, $schedule);
        }
    }

    public function runConfigSchedule(BackupConfig $config, Schedule $schedule)
    {
        try {
            $runner = $config->makeRunner();
            $runner->schedule($schedule->call(function () use ($runner) {
                $runner->execute();
            }));
        } catch (Throwable $t) {
            report($t);
        }
    }
}
