<?php

namespace App\Features\Backup\Schedules;

use Illuminate\Console\Scheduling\CallbackEvent;

abstract class BackupSchedule
{
    /**
     * Apply the schedule to the event to get the correct timing.
     */
    public abstract function apply(CallbackEvent $scheduleEvent);
}
