<?php

namespace App\Features\Backup\Schedules;

use Illuminate\Console\Scheduling\CallbackEvent;

class Daily extends BackupSchedule
{
    public function __construct(array $data)
    {
        $this->time = $data["time"] ?? "00:00";
    }
    /**
     * Apply the schedule to the event to get the correct timing.
     */
    public function apply(CallbackEvent $scheduleEvent)
    {
        $scheduleEvent->dailyAt($this->time);
    }
}
