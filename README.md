# Backup app
Collects multiple application backups into one system.

Can backup multiple sources to multiple destinations.

## 1. Backup targets (Apps)

These are "apps" that need to be backed up

## 2. Backup configurations

These are sets of options to be run in a schedule on a backup target

## 3. Backups
The registry of backups that were taken